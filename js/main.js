window.onload = function() {
  // let inpts = document.querySelectorAll('[type="text"]');
  let btn = document.querySelector('[type="button"]');
  let divRes = document.querySelector('div');
  btn.onclick = function() {
    ajaxPost('name=vasya');
  };
  // ===== ???????? ==== вот этот запрос POST
  function ajaxPost(param) {
    let request = new XMLHttpRequest();

    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        divRes.innerHTML = request.responseText;
      }
    };

    request.open('POST', '../back.php');
    request.setRequestHeader(
      'Content-type',
      'application/x-www-form-urlencoded',
    );
    request.send(param);
  }

  // ====== получаем и отслеживаем событие на клик ссылок меню
  let menu1 = document.querySelector('.menu1');
  let menu2 = document.querySelector('.menu2');
  let menu3 = document.querySelector('.menu33');
  let divInnerHtml = document.querySelector('div');

  menu1.addEventListener('click', function(e) {
    e.preventDefault();
    rendHtml('/html/main.html', function(data) {
      divInnerHtml.innerHTML = data;
    });
  });
  menu2.addEventListener('click', function(e) {
    e.preventDefault();
    rendHtml('/html/animate.html', function(data) {
      divInnerHtml.innerHTML = data;
    });
  });
  // ======наша функция вставки динамического html=======
  function rendHtml(url, callback) {
    let xml = new XMLHttpRequest();

    xml.onreadystatechange = function() {
      if (xml.readyState == 4 && xml.status == 200) {
        console.log(this);
        callback(xml.responseText);
      }
    };
    xml.open('GET', url);
    xml.send();
  }
  // ================

  // let obj = {
  //   name: 'Mike',
  //   surname: 'Mathew',
  //   nationality: 'Australian',
  //   languages: ['English', 'Spanish', 'French', 'Russian'],
  // };
  // let json = JSON.stringify(obj);
  // console.log(json);
};
